<?php

namespace App\Http\Controllers\Admin;

use App\Update;
use App\Device;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware(['auth:admin','clearance']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $updates=Update::all('id','device_id','buildversion','ziplink','changelog','xdathread')->sortByDesc("created_at");
        foreach ($updates as $key => $update) {
            $device=Device::where('id',$update->device_id)->first();
            $updates[$key]['devicemodel']=$device['model'];
            $updates[$key]['devicename']=$device['name'];
            unset($updates[$key]['device_id']);
        }
        return response()->json($updates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $update=new Update($request->all());
        try{
        $update->save();
        return response()->json([
            'msgtype'=>'success',
            'body'=>'Update Saved successfully.'
            ]);
        }catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            return response()->json([
            'msgtype'=>'error',
            'body'=>'Update Saving failed.error '.$errorCode
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function show(Update $update)
    {
        $update=Update::where('id',$id)->get(['id','devicemodel','buildversion','ziplink','changelog','xdathread'])->first();
        return response()->json($update);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $update=Update::find($id);
        return response()->json($update);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=Update::find($id);
        $inputs=$request->all();
        $update->update($inputs);
        $title = 'FireHound Update available';
        $body = 'Build ' . $request['build_version'];
        $device = $request['device_model'];
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = 'AAAAisGeVBo:APA91bFJrVs91Hmssv5VD3h8K8rRfNN_5u0vwjSbzx0eZn1sJEiR2FN4IlI9_ZfYzl0rNdgtqIkLb_zvmhsxYrGrO2TwgIUF78SrZh0QvG14yMtLNwY3_UHvxa5Dy0JcqxbNUAY8VGfO';
        $data=array('title'=>$title,'body'=>$body);
        $fields = array();
        $fields['data'] = $data;
        //to contents will be replaced by device ID
        $fields['to'] = '/topics/' . $device;
        //header with content_type api key
        $headers = array(
        'Content-Type:application/json',
        'Authorization:key='.$server_key
        );
            
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return response()->json([
            'msgtype'=>'success',
            'body'=>'Update updated successfully.'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Update::destroy($id);
        return response()->json([
            'msgtype'=>'success',
            'body'=>'Update deleted successfully.'
            ]);
    }
}
